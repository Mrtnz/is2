<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TopicContent extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'topic_contents';

    public function subject_topic(){
        return $this->belongsTo(SubjectTopic::class);
    }
}
