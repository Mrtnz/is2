<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategorySubject extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'sub_category_subject';

    protected $fillable = ['name'];

    /**
     * Relacion inversa con la tabla category_subject
     */
    public function category_subject(){
        return $this->belongsTo(CategorySubject::class);
    }
}
