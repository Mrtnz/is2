<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubjectTopic extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'subject_topics';

    /**
     * Relacion inversa con la table subject
     * Un tema pertenece a una materia
     */
    public function subject(){
        return $this->belongsTo(Subject::class);
    }

    public function topic_content(){
        return $this->hasOne(TopicContent::class);
    }

    public function topic_tests(){
        return $this->hasMany(TopicTest::class);
    }

    public function topic_videos(){
        return $this->hasMany(TopicVideos::class);
    }
}
