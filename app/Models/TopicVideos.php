<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TopicVideos extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'topic_videos';

    public function subject_topic(){
        return $this->belongsTo(SubjectTopic::class);
    }
}
