<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategorySubject extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'category_subject';

    protected $fillable = ['name'];

    /**
     * Relacion con la tabla sub_category_subject
     */
    public function sub_category_subject(){
        return $this->hasMany(SubCategorySubject::class);
    }
}
