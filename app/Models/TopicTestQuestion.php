<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TopicTestQuestion extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'topic_test_questions';

    public function topic_test(){
        return $this->belongsTo(TopicTest::class);
    }
}
