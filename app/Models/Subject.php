<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "subjects";

    /**
     * Relación con la tabla sub_category_subject
     * Una materia pertenece a una subcategoria
     */
    public function sub_category_subject(){
        return $this->belongsTo(SubCategorySubject::class);
    }

    /**
     * Relación con la tabla subject_topic
     * Una materia puede tener varios temas
     */
    public function subject_topic(){
        return $this->hasMany(SubjectTopic::class);
    }
}
