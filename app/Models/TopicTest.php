<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TopicTest extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'topic_tests';

    public function topic_test_question(){
        return $this->hasMany(TopicTestQuestion::class);
    }

    public function subject_topic(){
        return $this->belongsTo(SubjectTopic::class);
    }
}
