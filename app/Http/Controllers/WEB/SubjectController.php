<?php

namespace App\Http\Controllers\WEB;

use App\Http\Controllers\Controller;
use App\Models\Subject;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Inertia\Inertia;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        //
        $subjects = Subject::all();
        return Inertia::render('Admin/subject/SubjectComponent', ['subjects' => $subjects]);
    }

    public function _index()
    {
        //
        $subjects = Subject::all();
        //return Inertia::render('Admin/subject/SubjectComponent', ['subjects' => $subjects]);
        return response()->json($subjects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Subject
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        Validator::make( $request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
            'has_test' => ['required', 'boolean'],
            'subcategoryId' => ['required']
        ])->validate();

        $subject = new Subject();
        $subject->name = $request->name;
        $subject->description = $request->description;
        $subject->has_test = $request->has_test;
        $subject->sub_category_subject_id = $request->subcategoryId;

        if( $request->has('options') ){
            $subject->options = $request->options;
        } else {
            $subject->options = "{}";
        }

        $subject->save();

        return $subject;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        //Se procede a elminar la materia
        $subject = Subject::findOrFail($id);
        if( $subject->delete() ){
            return response()->json(["message" => "Se ha eliminado la materia."], 200);
        } else{
            return response()->json(['message' => "Ha ocurrido un error, intentelo nuevamente."], 400);
        }
    }
}
