<?php

namespace App\Http\Controllers\WEB;

use App\Actions\Fortify\CreateNewUser;
use App\Http\Controllers\Controller;
use App\Mail\TeacherInvitation;
use App\Models\User;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Inertia\Inertia;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $users = User::with('roles')->get();
        return Inertia::render('Admin/users/UsersComponent', ['users' => $users]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function _index(){
        $users = User::with('roles')->get();
        return response()->json($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required'
        ]);

        if( $validator->fails() ){
            return response()->json( $validator->errors() );
        }

        return DB::transaction( function() use ( $request ) {
            return tap(User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]), function( User $user ) use ( $request ) {
                try{
                    $user->ownedTeams()->save(Team::forceCreate([
                        'user_id' => $user->id,
                        'name' => explode(' ', $user->name, 2)[0]."'s Team",
                        'personal_team' => true,
                    ]));

                    $user->assignRole($request->rol);

                    $emailData = [ "email" => $request->email, "password" => $request->password, "rol" => $request->rol];

                    Mail::to( $user )->send( new TeacherInvitation( $emailData ) );
                } catch (\Exception $e){
                    return response()->json( [ "error" => $e->getMessage() ] );
                }
            });
        });

        //return CreateNewUser::create($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Inertia\Response
     */
    public function edit( $id )
    {
        $user = User::findOrFail($id);

        return Inertia::render('Admin/users/EditUserComponent',['_user' => $user]);

        //return response()->json($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'rol' => 'required'
        ]);
        if( $validator->fails() ){
            return response()->json($validator->errors());
        }

        /**
         * Se busca al usaurio
         */
        $user = User::findOrFail($id);

        /**
         * Se actualiza la información
         */
        $user->name = $request->name;
        $user->assignRole($request->rol);

        $user->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

//Roles Section

    public function indexRoles(){
        $roles = Role::all();
        return response()->json(['data'=> $roles], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createRole( Request $request ){
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if( $validator->fails() ){
            return response()->json($validator->errors(), 400);
        }

        $rol = Role::create(['guard_name' => 'web', 'name' => $request->name]);
        return response()->json($rol, 200);
    }

    public function generatePassword(){
        $randomPassword = Str::random(8);

        return response()->json(["password"=> $randomPassword], 200);
    }

    public function sendMailVerificationUser( Request $request ){
        //Mail::to( $request->mail )->send( new TeacherInvitation() );
        return response()->json(['test'=>'test']);
    }
}
