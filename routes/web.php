<?php

use App\Http\Controllers\WEB\CategoryController;
use App\Http\Controllers\WEB\HomeController;
use App\Http\Controllers\WEB\SubCategoryController;
use App\Http\Controllers\WEB\SubjectController;
use App\Http\Controllers\WEB\SubjectTopicsController;
use App\Http\Controllers\WEB\TemasController;
use App\Http\Controllers\WEB\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home.index');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia\Inertia::render('Dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->group(function (){
    Route::get('admin/users', [UsersController::class, 'index'] )->name('admin.users.index');
    Route::get('admin/_users', [UsersController::class, '_index'])->name('admin.users._index');
    Route::get('users/{id}/edit', [UsersController::class, 'edit'])->name('admin.users.edit');
    Route::post('admin/users', [UsersController::class, 'store'])->name('admin.users.store');
    Route::post('admin/users/{id}', [UsersController::class, 'update'])->name('admin.users.update');
    Route::get('admin/generate_password', [UsersController::class, 'generatePassword'])->name('admin.users.password');
    Route::post('admin/send/verification_mail', [UsersController::class, 'sendMailVerificationUser']);

    Route::get('admin/roles', [UsersController::class, 'indexRoles'])->name('admin.role.index');
    Route::post('admin/role', [UsersController::class, 'createRole'])->name('admin.role.create');

    Route::get('admin/subject',[SubjectController::class, 'index'])->name('admin.subject.index');
    Route::get('admin/_subject',[SubjectController::class, '_index'])->name('admin._subject.index');
    Route::post('admin/subject', [SubjectController::class, 'store'])->name('admin.subject.store');
    Route::get('admin/subject/{id}/delete', [SubjectController::class, 'destroy'])->name('admin.subject.destroy');

    Route::get('admin/category', [CategoryController::class, 'index'])->name('admin.category.index');
    Route::post('admin/category',[CategoryController::class, 'store'])->name('admin.category.store');

    Route::post('admin/sub-category',[SubCategoryController::class, 'store'])->name('admin.subcategory.store');
    Route::get('admin/sub-category', [SubCategoryController::class, 'index'])->name('admin.subcategory.index');

    Route::get('admin/temas', [TemasController::class, 'index'])->name('admin.temas.index');
    Route::get('admin/_temas', [TemasController::class, '_index'])->name('admin.temas._index');
    Route::post('admin/temas/{id}', [TemasController::class, 'update'])->name('admin.temas.update');

    Route::get('admin/topics', [SubjectTopicsController::class, 'index'])->name('admin.topics.index');
    Route::post('admin/topics', [SubjectTopicsController::class, 'store'])->name('admin.topics.store');
});
