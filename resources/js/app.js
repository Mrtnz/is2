require('./bootstrap');

import Vue from 'vue';
import Vuetify from 'vuetify';

import { InertiaApp } from '@inertiajs/inertia-vue';
import { InertiaForm } from 'laravel-jetstream';
import PortalVue from 'portal-vue';

import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'

Vue.mixin({ methods: { route } });
Vue.use(InertiaApp);
Vue.use(InertiaForm);
Vue.use(PortalVue);
Vue.use(Vuetify, {iconFont: 'mdi'});

const app = document.getElementById('app');
export const EventBus = new Vue();

new Vue({
    render: (h) =>
        h(InertiaApp, {
            props: {
                initialPage: JSON.parse(app.dataset.page),
                resolveComponent: (name) => require(`./Pages/${name}`).default,
            },
        }),
    vuetify: new Vuetify({
        theme: {
            themes: {
                light: {
                    primary: '#118509'
                }
            }
        }
    })
}).$mount(app);
